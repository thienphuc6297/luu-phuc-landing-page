import { Button as MuiButton } from '@material-ui/core';
import clsx from 'clsx';
import React from 'react';

import { useStyles } from './styles';

interface IButtonProps {
    onClick?: (e) => void;
    className?: string;
    disabled?: boolean;
}

const Button: React.FunctionComponent<IButtonProps> = ({ children, className, disabled, onClick }) => {
    const classes = useStyles();

    return (
        <MuiButton className={clsx(classes.root, className)} disabled={disabled} onClick={onClick}>
            {children}
        </MuiButton>
    );
};

export default Button;
