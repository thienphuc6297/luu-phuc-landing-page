import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.primary.main,
        fontFamily: theme.typography.boldPro,
        fontSize: '16px',
        textTransform: 'none',
        color: '#fff',
    },
}));
