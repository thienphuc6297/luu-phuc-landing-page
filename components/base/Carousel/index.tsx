import React from 'react';
import SwiperCore, { Autoplay } from 'swiper';
import { Swiper } from 'swiper/react';

// install Swiper modules
SwiperCore.use([Autoplay]);

type ICarouselProps = {
    slidesPerView?: number;
};

const Carousel: React.FunctionComponent<ICarouselProps> = ({ children, slidesPerView }) => {
    return (
        <Swiper
            slidesPerView={slidesPerView}
            autoplay={{
                delay: 2500,
                disableOnInteraction: false,
            }}
        >
            {children}
        </Swiper>
    );
};

export default Carousel;
