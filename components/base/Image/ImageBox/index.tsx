import clsx from 'clsx';
import React from 'react';

import { useStyles } from './styles';

interface IImageBoxProps {
    className?: string;
}

const ImageBox: React.FunctionComponent<IImageBoxProps> = ({ children, className }) => {
    const classes = useStyles();
    return <div className={clsx(classes.imageBox, className)}>{children}</div>;
};

export default ImageBox;
