import NextImage, { ImageProps } from 'next/image';
import React from 'react';

import ImageBox from './ImageBox';

type IImageProps = {
    src?: StaticImageData;
    alt?: string;
    layout?: string;
    objectFit?: string;
    objectPosition?: string;
} & ImageProps;

const Image: React.FunctionComponent<IImageProps> = ({
    alt,
    src,
    layout = 'fill',
    objectFit = 'contain',
    objectPosition = 'center',
    className,
}) => {
    return (
        <ImageBox className={className}>
            <NextImage src={src} alt={alt} layout={layout} objectFit={objectFit} objectPosition={objectPosition} />
        </ImageBox>
    );
};

export default Image;
