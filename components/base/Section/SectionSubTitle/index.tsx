import Typography from '@components/base/Typography';
import React from 'react';

import { useStyles } from './styles';

const SectionTitle: React.FunctionComponent = ({ children }) => {
    const classes = useStyles();

    return (
        <Typography className={classes.sectionSubTitle} component="h3">
            {children}
        </Typography>
    );
};

export default SectionTitle;
