import Typography from '@components/base/Typography';
import clsx from 'clsx';
import React from 'react';

import { useStyles } from './styles';

interface ISectionText {
    className?: string;
}

const SectionTitle: React.FunctionComponent<ISectionText> = ({ children, className }) => {
    const classes = useStyles();

    return (
        <Typography className={clsx(classes.sectionText, className)} component="p">
            {children}
        </Typography>
    );
};

export default SectionTitle;
