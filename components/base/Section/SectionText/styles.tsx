import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    sectionText: {
        fontFamily: theme.typography.regularPro,
        color: theme.palette.primary.main,
    },
}));
