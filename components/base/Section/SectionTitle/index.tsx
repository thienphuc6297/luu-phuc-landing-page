import Typography from '@components/base/Typography';
import clsx from 'clsx';
import React from 'react';

import { useStyles } from './styles';

interface ISectionTitleProps {
    className?: string;
}
const SectionTitle: React.FunctionComponent<ISectionTitleProps> = ({ children, className }) => {
    const classes = useStyles();

    return (
        <Typography className={clsx(classes.sectionTitle, className)} component="h1">
            {children}
        </Typography>
    );
};

export default SectionTitle;
