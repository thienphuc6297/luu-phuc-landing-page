import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    sectionTitle: {
        fontSize: '48px',
        fontFamily: theme.typography.boldPro,
        lineHeight: 1.2,
    },
}));
