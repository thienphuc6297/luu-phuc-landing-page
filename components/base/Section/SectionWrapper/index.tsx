import clsx from 'clsx';
import React from 'react';

import { useStyles } from './styles';

interface ISectionWrapperProps {
    id?: string;
    className?: string;
}

const SectionWrapper: React.FunctionComponent<ISectionWrapperProps> = ({ children, id, className }) => {
    const classes = useStyles();

    return (
        <section id={id} className={clsx(classes.sectionWrapper, className)}>
            {children}
        </section>
    );
};

export default SectionWrapper;
