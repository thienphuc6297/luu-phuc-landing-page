// Material
import { Typography as MuiTypography } from '@material-ui/core';
import clsx from 'clsx';
import React, { ElementType } from 'react';

import { useStyles } from './styles';

enum Variant {
    caption = 'caption',
}

interface ITypographyProps {
    component?: ElementType;
    variant?: Variant | string;
    className?: string;
}

const Typography: React.FunctionComponent<ITypographyProps> = ({ children, className, variant, component }) => {
    const classes = useStyles();

    return (
        <MuiTypography className={clsx(className, classes[variant])} component={component}>
            {children}
        </MuiTypography>
    );
};

export default Typography;
