import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    caption: {
        fontSize: 14,
        lineHeight: '16px',
        fontFamily: theme.typography.medium,
    },
}));
