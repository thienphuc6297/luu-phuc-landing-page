import clsx from 'clsx';
import React from 'react';

import { useStyles } from './styles';

interface IContainerProps {
    className?: string;
}

const Container: React.FunctionComponent<IContainerProps> = ({ children, className }) => {
    const classes = useStyles();

    return <div className={clsx(classes.container, className)}>{children}</div>;
};

export default Container;
