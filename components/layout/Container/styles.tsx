import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
    container: {
        width: '100%',
        maxWidth: '1280px',
        margin: '0 auto',
    },
}));
