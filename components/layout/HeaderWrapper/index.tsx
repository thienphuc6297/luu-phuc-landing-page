import React from 'react';

import { useStyles } from './styles';

const HeaderWrapper: React.FunctionComponent = ({ children }) => {
    const classes = useStyles();

    return <div className={classes.headerWrapper}>{children}</div>;
};

export default HeaderWrapper;
