import React from 'react';

import { useStyles } from './styles';

const MainWrapper: React.FunctionComponent = ({ children }) => {
    const classes = useStyles();

    return <div className={classes.mainWrapper}>{children}</div>;
};

export default MainWrapper;
