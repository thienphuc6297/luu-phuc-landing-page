/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

import { useStyles } from './styles';

type INavBarItem = {
    active?: boolean;
};

const NavItem: React.FunctionComponent<INavBarItem> = ({ active, children }) => {
    const classes = useStyles({ active });

    return <a className={classes.navItem}>{children}</a>;
};

export default NavItem;
