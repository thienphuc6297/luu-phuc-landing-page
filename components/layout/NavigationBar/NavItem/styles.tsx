import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    navItem: {
        display: 'flex',
        alignItems: 'center',
        fontFamily: theme.typography.regularPro,
        fontSize: '16px',
        color: '#969696',
        cursor: 'pointer',
        '&:hover': {
            color: theme.palette.primary.main,
            transitionTimingFunction: 'cubic-bezier(.25,.25,.75,.75)',
        },
    },
}));
