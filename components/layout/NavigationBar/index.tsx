/* eslint-disable jsx-a11y/anchor-is-valid */
import { Button, ImageBox, Section } from '@components';
// import { TimebeeLogo } from '@components/base/Icons';
import NextImage from 'next/image';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';

import headerNavLinks from './headerNavLinks.json';
import NavItem from './NavItem';
import { useStyles } from './styles';

const { SectionWrapper } = Section;

const NavigationBar: React.FunctionComponent = () => {
    const classes = useStyles();
    const router = useRouter();

    return (
        <SectionWrapper id="navbar">
            <div className={classes.navigationBar}>
                <NextLink href="/">
                    {/* <TimebeeLogo height="28" className={classes.logo} /> */}
                    <ImageBox className={classes.logoBox}>
                        <NextImage
                            alt="familia-logo"
                            src="/images/familia-logo.svg"
                            objectFit="contain"
                            objectPosition="center"
                            layout="fill"
                        />
                    </ImageBox>
                </NextLink>
                <div className={classes.headerNavLinksWrapper}>
                    {headerNavLinks.map((link) => (
                        <NextLink key={link.title} href={link.pathname} passHref>
                            <NavItem active={router.pathname === link.pathname}>{link.title}</NavItem>
                        </NextLink>
                    ))}

                    {/* Contact us */}
                    <Button className={classes.button}>Contact us</Button>
                </div>
            </div>
        </SectionWrapper>
    );
};

export default NavigationBar;
