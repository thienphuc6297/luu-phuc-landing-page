import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
    navigationBar: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    logoBox: {
        width: 80,
        height: 60,
        margin: 'auto 0',
    },
    logo: {
        margin: 'auto 0',
    },
    headerNavLinksWrapper: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '50%',
    },
    button: {
        width: '24%',
        height: '45px',
        margin: 'auto 0',
        fontSize: '16px',
        borderRadius: '2px',
        padding: '10px 20px',
        '& .MuiButton-label': {
            margin: 'auto',
        },
    },
}));
