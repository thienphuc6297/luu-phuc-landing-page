import clsx from 'clsx';
import React from 'react';

import { useStyles } from './styles';

type IWrapperProps = {
    className: string;
};
const Wrapper: React.FunctionComponent<IWrapperProps> = ({ children, className }) => {
    const classes = useStyles();

    return <div className={clsx(classes.wrapper, className)}>{children}</div>;
};

export default Wrapper;
