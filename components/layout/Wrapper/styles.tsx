import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
    wrapper: {
        display: 'flex',
        flexDirection: 'column',
        position: 'relative',
    },
}));
