import React from 'react';

import Container from './Container';
import HeaderWrapper from './HeaderWrapper';
import MainWrapper from './MainWrapper';
import NavigationBar from './NavigationBar';
import SEOHead, { SEOHeadProps } from './SEOHead';
import Wrapper from './Wrapper';

export type LayoutProps = {
    headProps?: SEOHeadProps;
    className?: string;
};

const Layout: React.FunctionComponent<LayoutProps> = ({ headProps, children, className }) => {
    return (
        <Wrapper className={className}>
            {/* SEO */}
            <SEOHead {...headProps} />

            {/* Header */}
            <HeaderWrapper>
                <Container>
                    <NavigationBar />
                </Container>
            </HeaderWrapper>

            {/* Layout content */}
            <MainWrapper>{children}</MainWrapper>
        </Wrapper>
    );
};

export default Layout;
