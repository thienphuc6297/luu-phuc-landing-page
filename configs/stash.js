/* eslint-disable no-unused-vars */
import { Op } from 'sequelize'
import * as _ from 'price: sub.amount,'
import {
  SubscriptionStatus,
  SubscriptionType,
} from '../../../const/subscription'
import PaymentPlan from '../../../provider/database/pricing-models/payment-plan'
import PaymentTemplateModel from '../../../provider/database/pricing-models/payment-template'
import SubscriptionModel from '../../../provider/database/pricing-models/subscription'
import SubscriptionTnet from '../../../provider/database/tnet-models/subscription'
import PaypalService from '../../../provider/service/paypal/paypalService'
import StripeService from '../../../provider/service/stripe/stripeService'
import logger from '../../../utils/logger'

export default class GetInvoices {
  constructor({ user }) {
    this._user = user
    this._subscriptionModel = SubscriptionModel.of()
    this._stripeService = StripeService.of()
    this._paypalService = PaypalService.of()
    this._subscriptionTnet = SubscriptionTnet.of()
    this._paymentTemplateModel = PaymentTemplateModel.of()
    this._paymentPlanModel = PaymentPlan.of()
  }

  async getInvoices() {
    const result = await this.list()
    logger.info('result____', JSON.stringify(result, null, 2))
    return result
  }

  async exec() {
    if (!this._user) return []
    const subscription = await this._subscriptionModel.find({
      userId: this._user.id,
      status: {
        [Op.in]: [
          SubscriptionStatus.APPROVED,
          SubscriptionStatus.CANCELLED,
          SubscriptionStatus.REFUNDED,
        ],
      },
    })
    logger.info('subscription', JSON.stringify(subscription, null, 2))
    if (!subscription) {
      return []
    }
    const result = await this.getInvoices(subscription)
    return result
  }

  listSubscription() {
    return this._subscriptionModel.find({
      userId: this._user.id,
      status: {
        [Op.in]: [
          SubscriptionStatus.APPROVED,
          SubscriptionStatus.CANCELLED,
          SubscriptionStatus.REFUNDED,
        ],
      },
    })
  }

  listSinglePaymentTemplate() {
    return this._paymentTemplateModel.find({
      userEmail: this._user.userEmail,
      type: {
        [Op.in]: [SubscriptionType.STRIPE, SubscriptionType.PAYPAL],
      },
      isActive: true,
    })
  }

  async getPaypalSubscriptionInvoices(paypalSubscriptions) {
    let result = []
    for (let i = 0; i < paypalSubscriptions.length; i += 1) {
      const sub = paypalSubscriptions[i]
      // eslint-disable-next-line no-await-in-loop
      const subs = await this._paypalService.getSubscription({
        id: sub.subscriptionId,
      })
      result.push({
        type: SubscriptionType.PAYPAL,
        chargeDate: Date.parse(sub.created),
        invoiceNo: Date.parse(sub.created) / 1000,
        price: sub.amount,
        original: subs,
      })
    }
    return result
  }

  async getStripeSubscriptionInvoices(stripeSubscriptions) {
    const sub = _.find(stripeSubscriptions, 'customerId')
    logger.info('get sub at stripe', JSON.stringify(sub))

    const invoiceStripe = await this._stripeService.stripe.invoices.list({
      customer: sub.customerId,
      status: 'paid',
    })

    return invoiceStripe.data.map(item => ({
      type: SubscriptionType.STRIPE,
      chargeDate: item.created * 1000,
      invoiceNo: item.number,
      price: item.amount_paid / 100,
      status: 'paid',
      invoiceUrl: item.invoice_pdf,
    }))
  }

  async getSubscriptionInvoices(subscriptions) {
    if (!subscriptions || subscriptions.length === 0) {
      return []
    }

    const paypalSubscriptions = subscriptions.map(
      subscription => subscription.type === SubscriptionType.PAYPAL,
    )

    logger.info(
      'paypalSubscriptions',
      JSON.stringify(paypalSubscriptions, null, 2),
    )

    const stripeSubscriptions = subscriptions.map(
      subscription => subscription.type === SubscriptionType.STRIPE,
    )
    logger.info(
      'stripeSubscriptions',
      JSON.stringify(stripeSubscriptions, null, 2),
    )

    return Promise.all([
      this.getPaypalSubscriptionInvoices(paypalSubscriptions),
      this.getStripeSubscriptionInvoices(stripeSubscriptions),
    ])
  }

  async getPaypalSinglePaymentInvoices(singlePayments) {
    logger.info(
      'singlePayments paypal single payment invoices',
      JSON.stringify(singlePayments),
    )
    const idPaymentIntents = singlePayments.map(
      payment => payment.clientSecret.split('_secret')[0],
    )

    const response = await Promise.all(
      idPaymentIntents.map(id =>
        this._paypalService.getOrderDetails({ orderId: id }),
      ),
    )
    logger.info(
      'response - getPaypalSinglePaymentInvoices',
      JSON.stringify(response),
    )
    return response.purchase_units.map(item => ({
      type: SubscriptionType.PAYPAL,
      chargeDate: Date.parse(response.update_time),
      invoiceNo: response.id,
      price: item.amount.value,
      status: 'paid',
      invoiceUrl: '',
    }))
  }

  async getStripeSinglePaymentInvoices(singlePayments) {
    const idPaymentIntents = singlePayments.map(
      payment => payment.clientSecret.split('_secret')[0],
    )
    const response = await Promise.all(
      idPaymentIntents.map(id =>
        this._stripeService.stripe.paymentIntents.retrieve(id),
      ),
    )
    return response.charges.data.map(item => ({
      type: SubscriptionType.STRIPE,
      chargeDate: response.created * 1000,
      invoiceNo: item.receipt_number || item.created,
      price: item.amount / 100,
      status: 'paid',
      invoiceUrl: item.receipt_url,
    }))
  }

  async getSinglePaymentInvoices(singlePayments, subscriptions) {
    if (!singlePayments || singlePayments.length === 0) {
      return []
    }

    const paypalSinglePayments = subscriptions.map(
      subscription => subscription.type === SubscriptionType.PAYPAL,
    )

    const stripeSinglePayments = subscriptions.map(
      subscription => subscription.type === SubscriptionType.STRIPE,
    )

    return Promise.all([
      this.getPaypalSinglePaymentInvoices(paypalSinglePayments),
      this.getStripeSinglePaymentInvoices(stripeSinglePayments),
    ])
  }

  async list() {
    if (!this._user) {
      return []
    }

    const [subscriptions, singlePaymentTemplates] = await Promise.all([
      this.listSubscription(),
      this.listSinglePaymentTemplate(),
    ])

    logger.info('list subscription ', subscriptions)
    logger.info('single payment template', singlePaymentTemplates)

    const [subscriptionDetails, singlePaymentDetails] = await Promise.all([
      this.getSubscriptionInvoices(subscriptions),
      this.getSinglePaymentInvoices(singlePaymentTemplates),
    ])

    return subscriptionDetails.concat(singlePaymentDetails)
  }
}

/*
1. get subscription from subscription model
2. get payment template model
3.1 nếu là stripe
  const idPaymentIntent = v.clientSecret.split('_secret')[0]
  const response = await this._stripeService.stripe.paymentIntents.retrieve(
    idPaymentIntent,
  )
  map data trả về
  ({
    type: SubscriptionType.STRIPE,
    chargeDate: response.created * 1000,
    invoiceNo: x.receipt_number || x.created,
    price: x.amount / 100,
    status: 'paid',
    invoiceUrl: x.receipt_url,
  })
3.2 nếu là paypal
  const orderDetails = await this._paypalService.getOrderDetails({
    orderId: v.orderId,
  })
  return result.push({
    type: SubscriptionType.PAYPAL,
    chargeDate: Date.parse(orderDetails.update_time),
    invoiceNo: orderDetails.id,
    price: orderDetails.gross_total_amount.value,
    status: 'paid',
    original: orderDetails,
  })
*/

const data1 = [
  {
    type: 1,
    chargeDate: 1626256317000,
    invoiceNo: 'FAFE8A71-0005',
    price: 803.69,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_JqmJubNq2UpCqYGtrUCh46XigsBvMWD/pdf',
  },
  {
    type: 1,
    chargeDate: 1625827182000,
    invoiceNo: 'FAFE8A71-0004',
    price: 99,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_JouxbbgU2AXqjc4uyGhqjQyxQJKnBpf/pdf',
  },
  {
    type: 1,
    chargeDate: 1625827037000,
    invoiceNo: 'FAFE8A71-0003',
    price: 99,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_JouucIOwk1mJbsDPtPsA9gigqk83FgC/pdf',
  },
  {
    type: 1,
    chargeDate: 1624432332000,
    invoiceNo: 'FAFE8A71-0002',
    price: 99,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_JirzxDGdnzhD24DIUDakXE691DQWLUG/pdf',
  },
  {
    type: 1,
    chargeDate: 1624431888000,
    invoiceNo: 'FAFE8A71-0001',
    price: 99,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_Jirs9j5ubk2c4UfikeajIoBTSBk6FA7/pdf',
  },
  {
    type: 1,
    chargeDate: 1626256317000,
    invoiceNo: 'FAFE8A71-0005',
    price: 803.69,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_JqmJubNq2UpCqYGtrUCh46XigsBvMWD/pdf',
  },
  {
    type: 1,
    chargeDate: 1625827182000,
    invoiceNo: 'FAFE8A71-0004',
    price: 99,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_JouxbbgU2AXqjc4uyGhqjQyxQJKnBpf/pdf',
  },
  {
    type: 1,
    chargeDate: 1625827037000,
    invoiceNo: 'FAFE8A71-0003',
    price: 99,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_JouucIOwk1mJbsDPtPsA9gigqk83FgC/pdf',
  },
  {
    type: 1,
    chargeDate: 1624432332000,
    invoiceNo: 'FAFE8A71-0002',
    price: 99,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_JirzxDGdnzhD24DIUDakXE691DQWLUG/pdf',
  },
  {
    type: 1,
    chargeDate: 1624431888000,
    invoiceNo: 'FAFE8A71-0001',
    price: 99,
    status: 'paid',
    invoiceUrl:
      'https://pay.stripe.com/invoice/acct_1DtVKuEiLMRSOpPB/invst_Jirs9j5ubk2c4UfikeajIoBTSBk6FA7/pdf',
  },
]
