import { createTheme } from '@material-ui/core/styles';
// Fonts
const AvenirNextLTProHeavyOTF = '/fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-Heavy.otf';
const AvenirNextLTProBoldOTF = '/fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-Bold.otf';
const AvenirNextLTProDemiBoldOTF = '/fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-Demi.otf';
const AvenirNextLTProMediumOTF = '/fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-Medium.otf';
const AvenirNextLTProRegularOTF = '/fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-Regular.otf';
const AvenirNextLTProUltLtOTF = '/fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-UltLt.otf';

/**
 * Material UI theme
 * https://material-ui.com/customization/palette/
 */

const AvenirNextLTProHeavy = {
    fontFamily: 'Avenir Next LT Pro Heavy',
    src: `url(${AvenirNextLTProHeavyOTF}) format("opentype")`,
};

const AvenirNextLTProBold = {
    fontFamily: 'Avenir Next LT Pro Bold',
    src: `url(${AvenirNextLTProBoldOTF}) format("opentype")`,
};

const AvenirNextLTProDemiBold = {
    fontFamily: 'Avenir Next LT Pro Demi Bold',
    src: `url(${AvenirNextLTProDemiBoldOTF}) format("opentype")`,
};

const AvenirNextLTProMedium = {
    fontFamily: 'Avenir Next LT Pro Medium',
    src: `url(${AvenirNextLTProMediumOTF}) format("opentype")`,
};

const AvenirNextLTProRegular = {
    fontFamily: 'Avenir Next LT Pro Regular',
    src: `url(${AvenirNextLTProRegularOTF}) format("opentype")`,
};

const AvenirNextLTProUltLt = {
    fontFamily: 'Avenir Next LT Pro Ul Lt',
    src: `url(${AvenirNextLTProUltLtOTF}) format("opentype")`,
};

declare module '@material-ui/core/styles/createTypography' {
    interface TypographyOptions {
        heavyPro: string;
        boldPro: string;
        demiBoldPro: string;
        mediumPro: string;
        regularPro: string;
        ltPro: string;
    }
    interface Typography {
        heavyPro: string;
        boldPro: string;
        demiBoldPro: string;
        mediumPro: string;
        regularPro: string;
        ltPro: string;
    }
}

const theme = createTheme({
    palette: {
        primary: { main: '#383B50' },
    },
    typography: {
        heavyPro: 'Avenir Next LT Pro Heavy',
        boldPro: 'Avenir Next LT Pro Bold',
        demiBoldPro: 'Avenir Next LT Pro Demi Bold',
        mediumPro: 'Avenir Next LT Pro Medium',
        regularPro: 'Avenir Next LT Pro Regular',
        ltPro: 'Avenir Next LT Pro Ul Lt',
    },
    overrides: {
        MuiInput: {
            input: {
                '&:-webkit-autofill': {
                    '-webkit-box-shadow': '0 0 0 30px white inset !important',
                },
            },
        },
        MuiCssBaseline: {
            '@global': {
                html: {
                    width: '100%',
                    height: '100%',
                },
                body: {
                    WebkitFontSmoothing: 'antialiased',
                    MozOsxFontSmoothing: 'grayscale',
                    backgroundColor: '#fff',
                    width: '100%',
                    height: '100%',
                },
                '@font-face': [
                    AvenirNextLTProHeavy,
                    AvenirNextLTProBold,
                    AvenirNextLTProDemiBold,
                    AvenirNextLTProMedium,
                    AvenirNextLTProRegular,
                    AvenirNextLTProUltLt,
                ],
            },
        },
    },
});

export default theme;
