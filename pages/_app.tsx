// Css
import '../styles/globals.css';
import 'swiper/swiper.scss';

// Material
import theme from '@configs/theme';
import { CssBaseline, ThemeProvider as MuiThemeProvider } from '@material-ui/core';
// Library
import { debounce } from 'lodash';
// Next
import { AppProps } from 'next/app';
import Router from 'next/router';
import nprogress from 'nprogress';

// Slim progress bars for Ajax'y applications. Inspired by Google, YouTube, and Medium.
nprogress.configure({ showSpinner: false });
const start = debounce(nprogress.start, 300);
Router.events.on('routeChangeStart', start);
Router.events.on('routeChangeComplete', () => {
    start.cancel();
    nprogress.done();
    window.scrollTo({
        top: 0,
        left: 0,
        // behavior: 'smooth',
    });
});
Router.events.on('routeChangeError', () => {
    start.cancel();
    nprogress.done();
});

function App({ Component, pageProps, err }: AppProps & { err: Error }): JSX.Element {
    return (
        <MuiThemeProvider theme={theme}>
            <CssBaseline />
            <Component {...pageProps} err={err} />
        </MuiThemeProvider>
    );
}

export default App;
