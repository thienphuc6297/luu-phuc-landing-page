import Layout from '@components/layout';
import { Introduction } from '@sections/HomePage';
import React from 'react';

import { useStyles } from './styles';

const HomePage: React.FunctionComponent = () => {
    const classes = useStyles();

    return (
        <Layout className={classes.wrapperHomePageBackground}>
            <Introduction />
        </Layout>
    );
};

export default HomePage;
