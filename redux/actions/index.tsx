import { createAction, PrepareAction } from '@reduxjs/toolkit';
import { isEqual } from 'lodash';

import { IError, ThingsLoadResponse } from '../types';

const prepareError: PrepareAction<IError> = (error: Error) => {
    if (error instanceof Error) {
        return {
            payload: {
                message: error.message,
                stack: error.stack,
            },
        };
    } else if (isEqual(typeof error, 'string')) {
        return {
            payload: {
                message: error,
            },
        };
    } else {
        return { payload: error };
    }
};

// Thing load
export const thingsLoad = createAction<void>('THINGS_LOAD');
export const thingsLoadComplete = createAction<ThingsLoadResponse>('THINGS_LOAD_COMPLETE');
export const thingsLoadError = createAction('THINGS_LOAD_ERROR', prepareError);
