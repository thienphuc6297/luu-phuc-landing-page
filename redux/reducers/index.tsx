import { createReducer } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';
import { combineReducers } from 'redux';

import * as actions from '../actions';
import initialState from '../state';
import { AppState } from '../types';

const thingsPageReducer = createReducer(initialState.things_page, (builder) => {
    builder
        .addCase(actions.thingsLoad, (state) => {
            state.loading = true;
            state.error = null;
        })
        .addCase(actions.thingsLoadComplete, (state, action) => {
            state.loading = false;
            if (action.payload.error) state.error = action.payload.error;
            else state.things = action.payload.things;
        })
        .addCase(actions.thingsLoadError, (state, action) => {
            state.loading = false;
            state.error = action.payload;
        });
});

// Combined state
// -----------------------------------------------------------------------------
const combinedReducer = combineReducers({
    things_page: thingsPageReducer,
});

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const rootReducer = (state: AppState = initialState, action) => {
    switch (action.type) {
        case HYDRATE:
            return { ...state, ...action.payload };
        default:
            return combinedReducer(state, action);
    }
};

export default rootReducer;
