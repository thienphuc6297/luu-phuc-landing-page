import request from '@lib/request';
import { all, AllEffect, call, ForkEffect, put, takeLatest } from 'redux-saga/effects';

import * as actions from '../actions';

function* thingsSaga() {
    try {
        const response = yield call(request);
        yield put(actions.thingsLoadComplete(response));
    } catch (error) {
        yield put(actions.thingsLoadError(error));
    }
}

function* rootSaga(): Generator<AllEffect<ForkEffect<never>>, void, unknown> {
    yield all([takeLatest(actions.thingsLoad.type, thingsSaga)]);
}

export default rootSaga;
