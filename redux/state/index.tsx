import { AppState } from '../types';

const initialState: AppState = {
    things_page: {
        loading: false,
        things: null,
        error: null,
    },
};

export default initialState;
