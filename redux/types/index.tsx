import { CombinedState } from 'redux';

// Common
// -----------------------------------------------------------------------------
export interface IError {
    message: string;
    stack?: string;
}

// Models
// -----------------------------------------------------------------------------

export type Thing = {
    word: string;
    pronunciations: string;
    definition: string;
};

// Input/response
// -----------------------------------------------------------------------------

export type ThingsLoadResponse = {
    success: boolean;
    error: IError;
    things: Thing[];
};

// Reducer states
// -----------------------------------------------------------------------------

export type ThingsPageState = {
    things: Thing[];
    loading: boolean;
    error: IError;
};

export type AppState = CombinedState<{
    things_page: ThingsPageState;
}>;
