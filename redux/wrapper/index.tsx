import { configureStore } from '@reduxjs/toolkit';
import { createWrapper } from 'next-redux-wrapper';
import createSagaMiddleware from 'redux-saga';

import rootReducer from '../reducers';
import rootSaga from '../sagas';
import initialState from '../state';

declare module 'redux' {
    export interface Store {
        sagaTask: unknown;
    }
}

const USE_DEV_TOOLS = process.env.NODE_ENV !== 'production';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const makeStore = () => {
    const sagaMiddleware = createSagaMiddleware();

    const store = configureStore({
        reducer: rootReducer,
        preloadedState: initialState,
        middleware: [sagaMiddleware],
        devTools: USE_DEV_TOOLS,
    });

    store.sagaTask = sagaMiddleware.run(rootSaga);

    return store;
};

export const reduxWrapper = createWrapper(makeStore);
