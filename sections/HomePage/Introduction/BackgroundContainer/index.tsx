import { Carousel, ImageBox, Section } from '@components';
import NextImage from 'next/image';
import React from 'react';
import { SwiperSlide } from 'swiper/react';

import { introductions } from './introductions_json';
import { useStyles } from './styles';

const { SectionTitle, SectionText } = Section;

const BackgroundContainer: React.FunctionComponent = () => {
    const classes = useStyles();

    return (
        <div className={classes.backgroundContainer}>
            <Carousel slidesPerView={1}>
                {introductions.map((item) => (
                    <SwiperSlide key={item?.id}>
                        <SectionTitle className={classes.title}>{item?.title}</SectionTitle>
                        <SectionText className={classes.caption}>{item?.caption}</SectionText>
                        <SectionText className={classes.caption}>{item?.caption}</SectionText>
                        <SectionText className={classes.caption}>{item?.caption}</SectionText>
                    </SwiperSlide>
                ))}
            </Carousel>
            <ImageBox className={classes.background}>
                <NextImage
                    src="/images/team-work-background.svg"
                    objectFit="contain"
                    objectPosition="top right"
                    layout="fill"
                />
            </ImageBox>
        </div>
    );
};

export default BackgroundContainer;
