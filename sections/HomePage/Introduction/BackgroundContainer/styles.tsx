import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    backgroundContainer: {
        display: 'flex',
        position: 'relative',
        marginBottom: '72px',
        '& .swiper-container': {
            width: '100%',
        },
    },
    background: {
        width: '1280px',
        height: '600px',
        position: 'relative',
    },
    title: {
        width: '100%',
        marginBottom: 24,
        textAlign: 'left',
        color: theme.palette.primary.main,
    },
    caption: {
        width: '100%',
        textAlign: 'left',
        marginBottom: '48px',
    },
}));
