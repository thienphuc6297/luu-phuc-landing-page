/* eslint-disable react/no-unescaped-entities */
import { Section } from '@components';
import React from 'react';

import { useStyles } from './styles';

const { SectionTitle, SectionText } = Section;

const HelloContainer: React.FunctionComponent = () => {
    const classes = useStyles();

    return (
        <div className={classes.helloContainer}>
            <div>
                <SectionTitle className={classes.title}>Hello! We're New Generation</SectionTitle>
                <SectionText className={classes.caption}>
                    If you’re looking for a software company to transform your business to the next level, you've found
                    the right partner to help.
                </SectionText>

                <SectionText className={classes.caption}>
                    We are strongly recommended by technical and non-technical leaders in various industries including
                    retail, ecommerce, real estate, legal, travel & hospitality,...
                </SectionText>
            </div>
            <div></div>
        </div>
    );
};

export default HelloContainer;
