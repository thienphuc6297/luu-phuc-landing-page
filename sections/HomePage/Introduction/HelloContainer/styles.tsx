import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    helloContainer: {},
    title: {
        width: '100%',
        marginBottom: 24,
        textAlign: 'left',
        color: theme.palette.primary.main,
    },
    caption: {
        width: '100%',
        textAlign: 'left',
        marginBottom: '48px',
    },
}));
