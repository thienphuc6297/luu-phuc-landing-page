import { Section } from '@components';
import Container from '@components/layout/Container';
import React from 'react';

import BackgroundContainer from './BackgroundContainer';
import HelloContainer from './HelloContainer';
import { useStyles } from './styles';

const { SectionWrapper } = Section;

const Introduction: React.FunctionComponent = () => {
    const classes = useStyles();

    return (
        <SectionWrapper id="introduction" className={classes.sectionWrapper}>
            <Container className={classes.container}>
                <BackgroundContainer />

                <HelloContainer />
            </Container>
        </SectionWrapper>
    );
};

export default Introduction;
