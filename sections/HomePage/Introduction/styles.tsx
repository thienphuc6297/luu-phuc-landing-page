import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
    sectionWrapper: {
        padding: '100px 0',
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        overflow: 'hidden',
        width: '100%',
        position: 'relative',
    },
}));
